import React from 'react'

import { AlertBanner, TextInput } from '@habx/ui-core'

import { SurfaceContainer } from './SurfaceField.style'

const SurfaceField = ({ surface, error, handleSurfaceChange }) => {
  return (
    <SurfaceContainer>
      {error && (
        <AlertBanner
          message="Enter a surface area in the range of 25 - 90 m²"
          open
        />
      )}
      <TextInput
        placeholder="min: 25 - max: 90"
        label="Surface area"
        elementLeft="m²"
        onChange={handleSurfaceChange}
        value={surface}
      />
    </SurfaceContainer>
  )
}

export default SurfaceField
