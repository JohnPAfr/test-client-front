/* eslint-disable */

import { Select } from '@habx/ui-core'
import React from 'react'

import { TypologyContainer } from './TypologySelect.style'

const SELECT_OPTIONS: {
    label: string,
    value: string
}[] = [
    {
        label: "1",
        value: "1"
    },
    {
        label: "2",
        value: "2"
    },
    {
        label: "3",
        value: "3"
    },
    {
        label: "4",
        value: "4"
    },
    {
        label: "5",
        value: "5"
    },
    {
        label: "6",
        value: "6"
    },
]

const TypologySelect = ({typology, handleSelectChange}) => {


    return (
        <TypologyContainer>
            <Select
                label="How many rooms do you want?"
                value={typology}
                onChange={handleSelectChange}
                options={SELECT_OPTIONS}
            />            
        </TypologyContainer>
    )
}

export default TypologySelect
