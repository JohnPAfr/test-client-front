import { useMutation, useQuery } from '@apollo/client'
import React from 'react'
import { useState } from 'react'

import { Button, Title, Text } from '@habx/ui-core'

import BudgetField from './BudgetField'
import ExposuresCheck from './ExposuresCheck'
import { projectMutation, projectQuery } from './Setup.query'
import { SetupContainer, Form } from './Setup.style'
import SurfaceField from './SurfaceField'
import { project } from './types/project'
import TypologySelect from './TypologySelect'

const Setup = () => {
  const projectResponse = useQuery<project>(projectQuery)
  const [upsertUpdate] = useMutation(projectMutation)

  const [budget, setBudget] = useState<string>('')
  const [surface, setSurface] = useState<string>('')
  const [typology, setTypology] = useState<string>('1')
  const [exposures, setExposures] = useState<{
    north: boolean
    east: boolean
    south: boolean
    west: boolean
  }>({
    north: false,
    east: false,
    south: false,
    west: false,
  })
  const [budgetError, setBudgetError] = useState<boolean>(true)
  const [surfaceError, setSurfaceError] = useState<boolean>(true)
  const [exposuresError, setExposuresError] = useState<boolean>(true)
  const [displayError, setDisplayError] = useState<boolean>(false)

  const regex: RegExp = new RegExp('[0-9]*', 'g')

  const handleBudgetChange = (e: any) => {
    if (regex.test(e.currentTarget.value)) {
      setBudget(e.target.value)
    }
  }

  const handleSurfaceChange = (e: any) => {
    if (regex.test(e.currentTarget.value)) {
      setSurface(e.currentTarget.value)
    }
  }

  const handleSelectChange = (e: string) => {
    setTypology(e)
  }

  const handleExposuresChange = (exposure: string) => {
    setExposures((state) => {
      return {
        ...state,
        [exposure]: !state[exposure],
      }
    })
  }

  const handleSetupSubmit = (e: any) => {
    e.preventDefault()
    setDisplayError(true)
    const projectSetup: {
      budget: number | null
      surface: number | null
      exposures: string[]
      typology: number
    } = validateProjectSetup()

    if (
      projectSetup.budget &&
      projectSetup.surface &&
      projectSetup.exposures &&
      projectSetup.typology
    ) {
      upsertUpdate({
        variables: {
          setup: {
            budget: projectSetup.budget,
            surface: projectSetup.surface,
            exposures: projectSetup.exposures,
            typology: projectSetup.typology,
          },
        },
      })

      resetValues()
      resetErrors()
    }
  }

  const validateProjectSetup = () => {
    const resBudget: number | null = isBudgetInRange()
    const resSurface: number | null = isSurfaceInRange()
    const resExposures: string[] = isExposuresValid()
    const resTypology: number = Number(typology)

    return {
      budget: resBudget,
      surface: resSurface,
      exposures: resExposures,
      typology: resTypology,
    }
  }

  const isBudgetInRange = () => {
    const resBudget = Number(budget.replace(/\s/g, ''))
    if (resBudget >= 150000 && resBudget <= 800000) {
      setBudgetError(false)
      return resBudget
    } else {
      setBudgetError(true)
      return null
    }
  }

  const isSurfaceInRange = () => {
    if (Number(surface) >= 25 && Number(surface) <= 90) {
      setSurfaceError(false)
      return Number(surface)
    } else {
      setSurfaceError(true)
      return null
    }
  }

  const isExposuresValid = () => {
    const resExposures: string[] = Object.keys(exposures).filter(
      (exposure) => exposures[exposure]
    )
    if (resExposures.length === 0) setExposuresError(true)
    else {
      setExposuresError(false)
      return resExposures
    }
  }

  const resetValues = () => {
    setBudget('')
    setSurface('')
    setExposures({
      north: false,
      east: false,
      south: false,
      west: false,
    })
    setTypology('1')
  }
  const resetErrors = () => {
    setBudgetError(false)
    setSurfaceError(false)
    setExposuresError(false)
  }

  return (
    <SetupContainer>
      <Form onSubmit={handleSetupSubmit}>
        <Title type="header">{projectResponse.data?.project?.name}</Title>
        <br />

        <BudgetField
          budget={budget}
          error={displayError && budgetError}
          handleBudgetChange={handleBudgetChange}
        />
        <br />

        <SurfaceField
          surface={surface}
          error={displayError && surfaceError}
          handleSurfaceChange={handleSurfaceChange}
        />

        <TypologySelect
          typology={typology}
          handleSelectChange={handleSelectChange}
        />

        <Text>Which exposures would you prefer ? Pick at least one</Text>
        <ExposuresCheck
          exposures={exposures}
          error={displayError && exposuresError}
          handleExposuresChange={handleExposuresChange}
        />
        <Button type="submit">Submit</Button>
      </Form>
    </SetupContainer>
  )
}

export default Setup
