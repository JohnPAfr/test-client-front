import styled from 'styled-components'

export const SetupContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  height: 100vh;
`

export const Form = styled.form`
  display: flex;
  flex-direction: column;
`

export const CheckboxContainer = styled.div`
  display: flex;
  justify-content: space-between;
`
