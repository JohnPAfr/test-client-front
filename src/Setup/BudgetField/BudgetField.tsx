/* eslint-disable */

import { AlertBanner, TextInput } from '@habx/ui-core'
import React from 'react'

import { BudgetContainer } from './BudgetField.style'

const BudgetField = ({budget, error, handleBudgetChange}) => {

    return (
        <BudgetContainer>
            { error && 
            <AlertBanner 
                message="Enter a budget in the range of €150 000 - €800 000" 
                open />}
            <TextInput
                placeholder="min: 150 000 - max: 800 000"
                label="What's your budget? (€150 000 - €800 000 )"
                elementLeft="€"
                onChange={handleBudgetChange}
                value={budget}
            />
        </BudgetContainer>
    )
}

export default BudgetField
