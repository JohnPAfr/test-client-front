import React from 'react'

import { AlertBanner, Checkbox } from '@habx/ui-core'

import { ExposuresContainer } from './Exposures.style'

const ExposuresCheck = ({ exposures, error, handleExposuresChange }) => {
  return (
    <>
      {error && (
        <AlertBanner message="You have to select at least one exposure" open />
      )}
      <br />
      <ExposuresContainer>
        <Checkbox
          label="North"
          onChange={() => handleExposuresChange('north')}
          checked={exposures.north}
        />
        <Checkbox
          label="East"
          onChange={() => handleExposuresChange('east')}
          checked={exposures.east}
        />
        <Checkbox
          label="South"
          onChange={() => handleExposuresChange('south')}
          checked={exposures.south}
        />
        <Checkbox
          label="West"
          onChange={() => handleExposuresChange('west')}
          checked={exposures.west}
        />
      </ExposuresContainer>
    </>
  )
}

export default ExposuresCheck
