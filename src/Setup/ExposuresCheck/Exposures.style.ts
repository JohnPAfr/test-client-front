import styled from 'styled-components'

export const ExposuresContainer = styled.div`
  display: flex;
  justify-content: space-between;
  flex-grow: 1;
  width: 100%;

  margin-bottom: 30px;
`
