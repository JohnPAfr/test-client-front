import React from 'react'

import { AlertBanner, TextInput } from '@habx/ui-core'

import { LabelText } from '../Setup.style'

import { BudgetContainer } from './BudgetField.style'

const BudgetField = ({ budget, error, handleBudgetChange }) => {
  return (
    <BudgetContainer>
      <LabelText type="large">Quel est votre budget ?</LabelText>

      {error && (
        <AlertBanner
          message="Entrez une valeur comprise entre 150000 et 800000"
          open
        />
      )}
      <TextInput
        placeholder="min: 150000 - max: 800000"
        elementRight="€"
        onChange={handleBudgetChange}
        value={budget}
        style={{ width: 'initial', marginTop: '30px' }}
      />
    </BudgetContainer>
  )
}

export default BudgetField
