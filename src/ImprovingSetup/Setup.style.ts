import styled, { createGlobalStyle } from 'styled-components'

import { Text } from '@habx/ui-core'

export const GlobalStyle = createGlobalStyle`
  body {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
  }
`

export const SetupContainer = styled.div`
  position: relative;

  display: flex;
  justify-content: center;
  align-items: center;

  height: 100vh;
`

export const HeaderContainer = styled.div`
  position: absolute;
  top: 0;
  left: 0;

  width: 100%
  height: 10px;

  background-color: red;
`

export const Form = styled.form`
  display: flex;
  flex-direction: column;

  width: 600px;
`

export const CheckboxContainer = styled.div`
  display: flex;
  justify-content: space-between;
`

export const StepContainer = styled.div`
  display: flex;
  justify-content: space-between;
`

export const LabelText = styled(Text)`
  margin-bottom: 30px;
`
