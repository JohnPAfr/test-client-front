import gql from 'graphql-tag'

export const projectQuery = gql`
  query project {
    project {
      id
      name
      properties {
        priceRange {
          min
          max
        }
        surfaceRange {
          min
          max
        }
        exposures
        typologies
      }
    }
  }
`

export const projectMutation = gql`
  mutation upsertSetup($setup: SetupInput) {
    upsertSetup(setup: $setup) {
      setup
    }
  }
`

/* mutation upsertSetup(
  $budget: Int!,
  $surface: Int!,
  $exposures: [String]!,
  $typology: Int!,
) {
  upsertSetup(
    budget: $budget,
    surface: $surface,
    exposures: $exposures,
    typology: $typology
  ) {
    budget
    surface
    exposures
    typologie
  }
} */
