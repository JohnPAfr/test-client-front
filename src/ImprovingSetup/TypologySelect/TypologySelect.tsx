import React from 'react'

import { Select } from '@habx/ui-core'

import { LabelText } from '../Setup.style'

import { TypologyContainer } from './TypologySelect.style'

const TypologySelect = ({ typology, typologies, handleTypologyChange }) => {
  const selectOptions: {
    label: string
    value: string
  }[] = typologies.map((typo) => {
    return { label: typo, value: typo }
  })

  return (
    <TypologyContainer>
      <LabelText type="large">Combien de pièces désirez vous ?</LabelText>
      <Select
        value={typology}
        onChange={handleTypologyChange}
        options={selectOptions}
      />
    </TypologyContainer>
  )
}

export default TypologySelect
