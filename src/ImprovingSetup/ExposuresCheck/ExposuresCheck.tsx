import React from 'react'

import { AlertBanner, Checkbox } from '@habx/ui-core'

import { LabelText } from '../Setup.style'

import {
  ExposuresCheckboxContainer,
  ExposuresContainer,
} from './Exposures.style'

const ExposuresCheck = ({ exposures, error, handleExposuresChange }) => {
  return (
    <ExposuresContainer>
      <LabelText type="large">
        Quelle serait l'exposition parfaite de votre bien ?
      </LabelText>

      {error && (
        <AlertBanner
          message="Vous devez choisir au moins une exposition"
          open
        />
      )}
      <br />
      <ExposuresCheckboxContainer>
        <Checkbox
          label="Nord"
          onChange={() => handleExposuresChange('north')}
          checked={exposures.north}
        />
        <Checkbox
          label="Est"
          onChange={() => handleExposuresChange('east')}
          checked={exposures.east}
        />
        <Checkbox
          label="Sud"
          onChange={() => handleExposuresChange('south')}
          checked={exposures.south}
        />
        <Checkbox
          label="Ouest"
          onChange={() => handleExposuresChange('west')}
          checked={exposures.west}
        />
      </ExposuresCheckboxContainer>
    </ExposuresContainer>
  )
}

export default ExposuresCheck
