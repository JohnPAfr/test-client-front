import styled from 'styled-components'

export const ExposuresContainer = styled.div`
  padding: 64px 0;
`

export const ExposuresCheckboxContainer = styled.div`
  display: flex;
  justify-content: space-between;
  flex-grow: 1;
  width: 100%;
`
