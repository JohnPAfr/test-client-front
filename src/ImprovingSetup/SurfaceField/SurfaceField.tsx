import React from 'react'

import { AlertBanner, TextInput } from '@habx/ui-core'

import { LabelText } from '../Setup.style'

import { SurfaceContainer } from './SurfaceField.style'

const SurfaceField = ({ surface, error, handleSurfaceChange }) => {
  return (
    <SurfaceContainer>
      <LabelText type="large">
        Quelle est la surface idéale de votre bien ?
      </LabelText>
      {error && (
        <AlertBanner message="Entrez une valeur comprise entre 25 et 90" open />
      )}
      <TextInput
        placeholder="min: 25 - max: 90"
        elementRight="m²"
        onChange={handleSurfaceChange}
        value={surface}
        style={{ width: 'initial', marginTop: '30px' }}
      />
    </SurfaceContainer>
  )
}

export default SurfaceField
