import { useMutation, useQuery } from '@apollo/client'
import React from 'react'
import { useState } from 'react'

import { Button } from '@habx/ui-core'

import BudgetField from './BudgetField'
import ExposuresCheck from './ExposuresCheck'
import Header from './Header'
import { projectMutation, projectQuery } from './Setup.query'
import { GlobalStyle, SetupContainer, Form, StepContainer } from './Setup.style'
import SurfaceField from './SurfaceField'
import {
  project,
  project_project_properties,
  project_project_properties_priceRange,
  project_project_properties_surfaceRange,
} from './types/project'
import TypologySelect from './TypologySelect'

const STEP_TEXT: string[] = ['budget', 'surface', 'pièces', 'expositions']

const Setup = () => {
  const projectResponse = useQuery<project>(projectQuery)
  const projectProperties: project_project_properties =
    projectResponse.data?.project?.properties
  const priceRange: project_project_properties_priceRange =
    projectProperties?.priceRange
  const surfaceRange: project_project_properties_surfaceRange =
    projectProperties?.surfaceRange

  const [upsertUpdate] = useMutation(projectMutation)

  const [stepCount, setStepCount] = useState<number>(1)
  const [budget, setBudget] = useState<string>('')
  const [surface, setSurface] = useState<string>('')
  const [typology, setTypology] = useState<number>(1)
  const [exposures, setExposures] = useState<{
    north: boolean
    east: boolean
    south: boolean
    west: boolean
  }>({
    north: false,
    east: false,
    south: false,
    west: false,
  })
  const [budgetError, setBudgetError] = useState<boolean>(true)
  const [surfaceError, setSurfaceError] = useState<boolean>(true)
  const [exposuresError, setExposuresError] = useState<boolean>(true)
  const [displayError, setDisplayError] = useState<boolean>(false)

  const regex: RegExp = new RegExp('[0-9]*', 'g')

  const handleStepCountClick = (e) => {
    const innerHTML = e.target.innerHTML
    if (innerHTML.includes('Précédent') && stepCount > 1) {
      setStepCount((state) => state - 1)
    }
    if (innerHTML.includes('Suivant') && stepCount < 4) {
      switch (stepCount) {
        case 1:
          if (isBudgetInRange()) {
            setStepCount((state) => state + 1)
            setBudgetError(false)
            setDisplayError(false)
          } else {
            setBudgetError(true)
            setDisplayError(true)
          }
          break
        case 2:
          if (isSurfaceInRange()) {
            setStepCount((state) => state + 1)
            setSurfaceError(false)
            setDisplayError(false)
          } else {
            setSurfaceError(true)
            setDisplayError(true)
          }
          break
        default:
          setStepCount((state) => state + 1)
      }
    }
  }

  const handleBudgetChange = (e: any) => {
    if (regex.test(e.currentTarget.value)) {
      setBudget(e.target.value)
    }
  }

  const handleSurfaceChange = (e: any) => {
    if (regex.test(e.currentTarget.value)) {
      setSurface(e.currentTarget.value)
    }
  }

  const handleTypologyChange = (e: number) => {
    setTypology(e)
  }

  const handleExposuresChange = (exposure: string) => {
    setExposures((state) => {
      return {
        ...state,
        [exposure]: !state[exposure],
      }
    })
  }

  const handleSetupSubmit = (e: any) => {
    e.preventDefault()
    setDisplayError(true)
    const projectSetup: {
      budget: number | null
      surface: number | null
      exposures: string[]
      typology: number
    } = validateProjectSetup()

    if (
      projectSetup.budget &&
      projectSetup.surface &&
      projectSetup.exposures &&
      projectSetup.typology
    ) {
      upsertUpdate({
        variables: {
          setup: {
            budget: projectSetup.budget,
            surface: projectSetup.surface,
            exposures: projectSetup.exposures,
            typology: projectSetup.typology,
          },
        },
      })

      resetValues()
      resetErrors()
    }
  }

  const validateProjectSetup = () => {
    const resBudget: number | null = isBudgetInRange()
    const resSurface: number | null = isSurfaceInRange()
    const resExposures: string[] = isExposuresValid()
    const resTypology: number = Number(typology)

    return {
      budget: resBudget,
      surface: resSurface,
      exposures: resExposures,
      typology: resTypology,
    }
  }

  const isBudgetInRange = () => {
    const resBudget = Number(budget.replace(/\s/g, ''))
    if (resBudget >= priceRange.min && resBudget <= priceRange.max) {
      setBudgetError(false)
      return resBudget
    } else {
      setBudgetError(true)
      return null
    }
  }

  const isSurfaceInRange = () => {
    if (
      Number(surface) >= surfaceRange.min &&
      Number(surface) <= surfaceRange.max
    ) {
      setSurfaceError(false)
      return Number(surface)
    } else {
      setSurfaceError(true)
      return null
    }
  }

  const isExposuresValid = () => {
    const resExposures: string[] = Object.keys(exposures).filter(
      (exposure) => exposures[exposure]
    )
    if (resExposures.length === 0) setExposuresError(true)
    else {
      setExposuresError(false)
      return resExposures
    }
  }

  const resetValues = () => {
    setBudget('')
    setSurface('')
    setExposures({
      north: false,
      east: false,
      south: false,
      west: false,
    })
    setTypology(1)
  }
  const resetErrors = () => {
    setBudgetError(false)
    setSurfaceError(false)
    setExposuresError(false)
  }

  return (
    <>
      <GlobalStyle />
      <SetupContainer>
        <Header
          loading={stepCount}
          stepText={STEP_TEXT[stepCount - 1]}
          name={projectResponse.data?.project?.name}
        />
        <Form onSubmit={handleSetupSubmit}>
          {stepCount === 1 && (
            <BudgetField
              budget={budget}
              error={displayError && budgetError}
              handleBudgetChange={handleBudgetChange}
            />
          )}

          {stepCount === 2 && (
            <SurfaceField
              surface={surface}
              error={displayError && surfaceError}
              handleSurfaceChange={handleSurfaceChange}
            />
          )}

          {stepCount === 3 && (
            <TypologySelect
              typology={typology}
              typologies={projectProperties.typologies}
              handleTypologyChange={handleTypologyChange}
            />
          )}

          {stepCount === 4 && (
            <>
              <ExposuresCheck
                exposures={exposures}
                error={displayError && exposuresError}
                handleExposuresChange={handleExposuresChange}
              />
            </>
          )}

          <StepContainer>
            {stepCount > 1 && (
              <Button data-action="back" onClick={handleStepCountClick}>
                Précédent
              </Button>
            )}
            {stepCount < 4 && (
              <Button
                data-action="back"
                onClick={handleStepCountClick}
                style={{ marginLeft: 'auto' }}
              >
                Suivant
              </Button>
            )}
            {stepCount === 4 && (
              <Button type="submit" style={{ marginBottom: '30px' }}>
                Valider
              </Button>
            )}
          </StepContainer>
        </Form>
      </SetupContainer>
    </>
  )
}

export default Setup
