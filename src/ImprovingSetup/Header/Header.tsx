import React from 'react'

import { LoadingBar, Title } from '@habx/ui-core'

import { HeaderContainer, TitlesContainer } from './Header.style'

const Header = ({ loading, stepText, name }) => {
  return (
    <HeaderContainer>
      <TitlesContainer>
        <Title type="regular">{`Vos besoins: ${stepText} (${loading}/4)`}</Title>
        <Title type="section">{name}</Title>
      </TitlesContainer>
      <LoadingBar
        loaded={loading}
        total={4}
        style={{
          position: 'absolute',
          bottom: 0,
          left: 0,
          width: '100%',
        }}
      />
    </HeaderContainer>
  )
}

export default Header
