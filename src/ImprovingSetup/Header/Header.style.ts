import styled from 'styled-components'

export const HeaderContainer = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;

  width: 100%;

  padding-top: 20px;
  padding-bottom: 20px;
`

export const TitlesContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  padding: 0 20px;
`
