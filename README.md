# Test Habx

## Notes

J'ai noté un problème avec l'attribut 'bare' du TextInput

## Résumé rendu

### Jour 1

Mon but principal était de rendre quelque chose de fonctionnel en utilisant les composants de votre lib ui-core.
Tout d'abord, j'ai exploré le projet puis j'ai réalisé un formulaire basique.

Il m'a fallu quelques manipulations de graphql pour arriver à utiliser la mutation. (Ce sont mes débuts avec Graphql)
La partie TS m'a posé un peu plus de problèmes. En effet, je n'ai pas très bien compris les types générés automatiquement, pareil pour le fichier Schema.graphql.
J'ai donc typé comme j'ai pu.

Pour le design, il n'y a rien de plus basique. N'étant pas familier aux projets en TS strict avec l'utilisation de styled-component, j'ai choisi de mettre le design de côté dans un premier temps.

### Jour 2

Reconstruction du formulaire en wizard form
Utilisation de la LoadingBar
Utilisation du createGlobalStyle de styled-component pour le margin du body
Utilisation des valeurs de la projectResponse (priceRange et surfaceRange)